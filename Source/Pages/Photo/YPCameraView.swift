//
//  YPCameraView.swift
//  YPImgePicker
//
//  Created by Sacha Durand Saint Omer on 2015/11/14.
//  Copyright © 2015 Yummypets. All rights reserved.
//

import UIKit
import Stevia

class YPCameraView: UIView, UIGestureRecognizerDelegate {
    
    let focusView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 90))
    let previewViewContainer = UIView()
    let buttonsContainer = UIView()
    let flipButton = UIButton()
    let shotButton = UIButton()
    let flashButton = UIButton()
    let timeElapsedLabel = UILabel()
    let progressBar = UIProgressView()
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)

    convenience init(overlayView: UIView? = nil) {
        self.init(frame: .zero)
        
        if let overlayView = overlayView {
            // View Hierarchy
            sv(
                previewViewContainer,
                overlayView,
                progressBar,
                timeElapsedLabel,
                flashButton,
                flipButton,
                buttonsContainer.sv(
                    shotButton,
                    activityIndicator
                )
            )
        } else {
            // View Hierarchy
            sv(
                previewViewContainer,
                progressBar,
                timeElapsedLabel,
                flashButton,
                flipButton,
                buttonsContainer.sv(
                    shotButton,
                    activityIndicator
                )
            )
        }
        
        // Layout
        let isIphone4 = UIScreen.main.bounds.height == 480
        let sideMargin: CGFloat = isIphone4 ? 20 : 0
        if YPConfig.onlySquareImagesFromCamera {
            if UIDevice.current.userInterfaceIdiom == .pad {
                previewViewContainer.top(0)
                previewViewContainer.centerHorizontally()
                previewViewContainer.height(100%)
                previewViewContainer.Width == previewViewContainer.Height
                
                buttonsContainer.fillHorizontally()
                buttonsContainer.height(100)
                buttonsContainer.Bottom == previewViewContainer.Bottom - 50
            } else {
                previewViewContainer.top(0)
                previewViewContainer.centerHorizontally()
                previewViewContainer.width(100%)
                previewViewContainer.heightEqualsWidth()
                
                buttonsContainer.fillHorizontally()
                buttonsContainer.height(100)
                buttonsContainer.Top == previewViewContainer.Bottom + 50
            }
        }
        else {
            previewViewContainer.fillContainer()
            
            buttonsContainer.fillHorizontally()
            buttonsContainer.height(100)
            buttonsContainer.Bottom == previewViewContainer.Bottom - 50
        }
        
        progressBar.Left == previewViewContainer.Left
        progressBar.Right == previewViewContainer.Right
        progressBar.Bottom == previewViewContainer.Bottom

        overlayView?.followEdges(previewViewContainer)

        |-(15+sideMargin)-flashButton.size(42)
        flashButton.Bottom == previewViewContainer.Bottom - 15

        flipButton.size(42)
        flipButton.Right == previewViewContainer.Right - 15
        flipButton.Bottom == previewViewContainer.Bottom - 15
        
        timeElapsedLabel.Right == previewViewContainer.Right - 15
        timeElapsedLabel.Top == previewViewContainer.Top + 15
        
        shotButton.centerVertically()
        shotButton.size(84).centerHorizontally()
        activityIndicator.centerVertically()
        activityIndicator.size(84).centerHorizontally()

        // Style
        backgroundColor = YPConfig.colors.photoVideoScreenBackgroundColor
        previewViewContainer.backgroundColor = UIColor.ypLabel
        timeElapsedLabel.style { l in
            l.textColor = .white
            l.text = "00:00"
            l.isHidden = true
            l.font = YPConfig.fonts.cameraTimeElapsedFont
        }
        progressBar.style { p in
            p.trackTintColor = .clear
            p.tintColor = .ypSystemRed
        }
        flashButton.setImage(YPConfig.icons.flashOffIcon, for: .normal)
        flipButton.setImage(YPConfig.icons.loopIcon, for: .normal)
        shotButton.setImage(YPConfig.icons.capturePhotoImage, for: .normal)
        activityIndicator.color = UIColor.gray
        activityIndicator.hidesWhenStopped = true
    }
}
