//
//  YPVideoCaptureHelperDelegate.swift
//  YPImagePicker
//
//  Created by Mykola Prodeus on 19.03.2020.
//  Copyright © 2020 Yummypets. All rights reserved.
//

import Foundation

public protocol YPVideoCaptureHelperDelegate: class {
    
    //some process view before it's captured
    func onStartProcessingVideo()
    func didCaptureVideo(_ url: URL)
    func videoRecordingProgress(progress: Float, timeElapsed: TimeInterval)
}
