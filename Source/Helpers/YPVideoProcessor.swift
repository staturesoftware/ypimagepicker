//
//  YPVideoProcessor.swift
//  YPImagePicker
//
//  Created by Nik Kov on 13.09.2018.
//  Copyright © 2018 Yummypets. All rights reserved.
//

import UIKit
import AVFoundation

/*
 This class contains all support and helper methods to process the videos
 */
class YPVideoProcessor {
    
    /// Creates an output path and removes the file in temp folder if existing
    ///
    /// - Parameters:
    ///   - temporaryFolder: Save to the temporary folder or somewhere else like documents folder
    ///   - suffix: the file name wothout extension
    static func makeVideoPathURL(temporaryFolder: Bool, fileName: String) -> URL {
        var outputURL: URL
        
        if temporaryFolder {
            let outputPath = "\(NSTemporaryDirectory())\(fileName).\(YPConfig.video.fileType.fileExtension)"
            outputURL = URL(fileURLWithPath: outputPath)
        } else {
            guard let documentsURL = FileManager
                .default
                .urls(for: .documentDirectory,
                      in: .userDomainMask).first else {
                        print("YPVideoProcessor -> Can't get the documents directory URL")
                        return URL(fileURLWithPath: "Error")
            }
            outputURL = documentsURL.appendingPathComponent("\(fileName).\(YPConfig.video.fileType.fileExtension)")
        }
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: outputURL.path) {
            do {
                try fileManager.removeItem(atPath: outputURL.path)
            } catch {
                print("YPVideoProcessor -> Can't remove the file for some reason.")
            }
        }
        
        return outputURL
    }
    
    /*
     Crops the video to square by video height from the top of the video.
     */
    static func cropToSquare(filePath: URL, completion: @escaping (_ outputURL: URL?) -> Void) {
        
        // output file
        let outputPath = makeVideoPathURL(temporaryFolder: true, fileName: "squaredVideoFromCamera")
        
        // input file
        let asset = AVAsset.init(url: filePath)
        let composition = AVMutableComposition.init()
        composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        // Prevent crash if tracks is empty
        if asset.tracks.isEmpty {
            return
        }
        
        // input clip
        let clipVideoTrack = asset.tracks(withMediaType: .video)[0]
        
        // make it square
        let currentOrientation = YPDeviceOrientationHelper.shared.currentDeviceOrientation
        let minSide = min(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width)
        let maxSide = max(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width)
        
        let videoComposition = AVMutableVideoComposition()
        if YPConfig.onlySquareImagesFromCamera {
            videoComposition.renderSize = CGSize(width: minSide, height: minSide)
        } else {
            videoComposition.renderSize = clipVideoTrack.naturalSize
        }
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
        
        // rotate to potrait
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let transTr: CGAffineTransform
        let rotateTr: CGAffineTransform
        
        switch currentOrientation {
        case .portrait:
            transTr = CGAffineTransform(translationX: (minSide - maxSide)/2.0, y: -minSide)
            rotateTr = CGAffineTransform(rotationAngle: .pi/2)
        case .portraitUpsideDown:
            transTr = CGAffineTransform(translationX: -minSide - (maxSide - minSide) / 2, y: 0)
            rotateTr = CGAffineTransform(rotationAngle: -.pi/2)
        case .landscapeLeft:
            transTr = CGAffineTransform(translationX: (minSide - maxSide)/2.0, y: 0)
            rotateTr = CGAffineTransform(rotationAngle: 0)
        case .landscapeRight:
            transTr = CGAffineTransform(translationX: -minSide - (maxSide - minSide) / 2, y: -minSide)
            rotateTr = CGAffineTransform(rotationAngle: .pi)
        default:
            transTr = CGAffineTransform(translationX: 0, y: 0)
            rotateTr = CGAffineTransform(rotationAngle: 0)
        }
        
        let finalTransform = transTr.concatenating(rotateTr)
        transformer.setTransform(finalTransform, at: CMTime.zero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
        
        // exporter
        let exporter = AVAssetExportSession.init(asset: asset, presetName: YPConfig.video.compression)
        exporter?.videoComposition = videoComposition
        exporter?.outputURL = outputPath
        exporter?.shouldOptimizeForNetworkUse = false
        exporter?.outputFileType = YPConfig.video.fileType
        
        exporter?.exportAsynchronously {
            if exporter?.status == .completed {
                DispatchQueue.main.async(execute: {
                    completion(outputPath)
                })
            }
        }
    }
}
